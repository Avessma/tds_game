// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_Game.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TDS_Game, "TDS_Game" );

DEFINE_LOG_CATEGORY(LogTDS_Game)
 