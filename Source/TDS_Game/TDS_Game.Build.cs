// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TDS_Game : ModuleRules
{
	public TDS_Game(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule", "PhysicsCore" });
    }
}
