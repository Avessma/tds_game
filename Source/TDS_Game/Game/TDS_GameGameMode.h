// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDS_GameGameMode.generated.h"

UCLASS(minimalapi)
class ATDS_GameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATDS_GameGameMode();

	void PlayerCharacterDead();
};



