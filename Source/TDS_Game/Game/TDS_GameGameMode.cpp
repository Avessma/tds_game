// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_GameGameMode.h"
#include "TDS_GamePlayerController.h"
#include "TDS_Game/Character/TDS_GameCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATDS_GameGameMode::ATDS_GameGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATDS_GamePlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}

void ATDS_GameGameMode::PlayerCharacterDead()
{

}
