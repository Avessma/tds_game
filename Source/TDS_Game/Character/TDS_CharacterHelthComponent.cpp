// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_Game/Character/TDS_CharacterHelthComponent.h"

void UTDS_CharacterHelthComponent::ChangeHealthValue(float ChangeValue)
{

	float CurrentDamage = ChangeValue * CoefDamage;

	if (Shield > 0.0f && ChangeValue < 0.0f)
	{
		ChangeShieldValue(ChangeValue);
		if (Shield <= 0.0f)
		{
			//FX
			//OnShieldOff.Broadcast();
		}
	}
	else
	{
		Super::ChangeHealthValue(ChangeValue);

		if (Health < 39.0f)
		{
			//FX
			OnHealthMin.Broadcast();
		}
	}
}

float UTDS_CharacterHelthComponent::GetCurrentShield()
{
	return Shield;
}

void UTDS_CharacterHelthComponent::ChangeShieldValue(float ChangeValue)
{
	//OnShieldOff.Broadcast();

	Shield += ChangeValue;

	if (Shield > 100.0f)
	{
		Shield = 100.0f;
	}
	else
	{
		if (Shield < 0.0f)
		{
			Shield = 0.0f;
		}
	}

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CollDownShieldTimer, this, &UTDS_CharacterHelthComponent::CoolDownShieldEnd, CoolDownShieldRecoverTime, false);

		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	}

	OnShieldChange.Broadcast(Shield, ChangeValue);
}

void UTDS_CharacterHelthComponent::CoolDownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRateTimer, this, &UTDS_CharacterHelthComponent::RecoveryShield, ShieldRecoverRate, true);
	}

}

void UTDS_CharacterHelthComponent::RecoveryShield()
{
	if (Health > 0)
	{
		float tmp = Shield;
		tmp = tmp + ShieldRecoverValue;
		if (tmp > 100.0f)
		{
			Shield = 100.0f;
			if (GetWorld())
			{
				GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
			}
		}
		else
		{
			Shield = tmp;
		}

		OnShieldChange.Broadcast(Shield, ShieldRecoverValue);
	}	
}
