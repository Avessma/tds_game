// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_Game/Weapons/Grenade_Default.h"
#include "Kismet/GameplayStatics.h"
#include <Kismet/KismetSystemLibrary.h>
#include <DrawDebugHelpers.h>


void AGrenade_Default::BeginPlay()
{
	Super::BeginPlay();

}

void AGrenade_Default::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerExplose(DeltaTime);


}

void AGrenade_Default::TimerExplose(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (TimerToExplose > TimeToExplose) 
		{
			Explose();
		}
		else
		{
			TimerToExplose += DeltaTime;
		}
	}
}

void AGrenade_Default::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AGrenade_Default::ImpactProjectile()
{
	TimerEnabled = true;
}

void AGrenade_Default::Explose()
{
	if (DebugExploseShow)
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), BulletSettings.BulletMinRadiusDamage, 12, FColor::Green, false, 12.0f);
		DrawDebugSphere(GetWorld(), GetActorLocation(), BulletSettings.BulletMaxRadiusDamage, 12, FColor::Red, false, 12.0f);
	}
	TimerEnabled = false;
	if (BulletSettings.ExploseFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), BulletSettings.ExploseFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
	}
	if (BulletSettings.ExploseSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), BulletSettings.ExploseSound, GetActorLocation());
	}

	TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		BulletSettings.ExploseMaxDamage,
		BulletSettings.ExploseMaxDamage * 0.2f,
		GetActorLocation(),
		100.0f,
		200.0f,
		5,
		NULL, IgnoredActor, this, nullptr);

	this->Destroy();
}
