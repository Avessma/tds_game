// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDS_Game/Weapons/BulletsDefault.h"
#include "Grenade_Default.generated.h"

/**
 * 
 */
UCLASS()
class TDS_GAME_API AGrenade_Default : public ABulletsDefault
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;


public:
	virtual void Tick(float DeltaTime) override;

	void TimerExplose(float DeltaTime);

	virtual void BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	virtual void ImpactProjectile() override;

	void Explose();

	bool TimerEnabled = false;
	float TimerToExplose = 0.0f;
	float TimeToExplose = 2.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	bool DebugExploseShow = false;
};
