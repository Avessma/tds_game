// Fill out your copyright notice in the Description page of Project Settings.


#include "BulletsDefault.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

int32 DebugExplodeShow = 0;
FAutoConsoleVariableRef CVARExplodeShow(TEXT("TDS.DebugExplode"), DebugExplodeShow, TEXT("Draw Dedug for Explode"), ECVF_Cheat);

// Sets default values
ABulletsDefault::ABulletsDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BulletCollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionSphere"));

	BulletCollisionSphere->SetSphereRadius(16.f);

	BulletCollisionSphere->bReturnMaterialOnMove = true;

	BulletCollisionSphere->SetCanEverAffectNavigation(false);

	RootComponent = BulletCollisionSphere;

	BulletMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BulletProjectileMesh"));
	BulletMesh->SetupAttachment(RootComponent);
	BulletMesh->SetCanEverAffectNavigation(false);

	BulletFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("BulletFX"));
	BulletFX->SetupAttachment(RootComponent);

	BulletProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("BulletProjectileMovement"));
	BulletProjectileMovement->UpdatedComponent = RootComponent;
	BulletProjectileMovement->InitialSpeed = 1.f;
	BulletProjectileMovement->MaxSpeed = 0.f;

	BulletProjectileMovement->bRotationFollowsVelocity = true;
	BulletProjectileMovement->bShouldBounce = true;
}

// Called when the game starts or when spawned
void ABulletsDefault::BeginPlay()
{
	Super::BeginPlay();
	
	BulletCollisionSphere->OnComponentHit.AddDynamic(this, &ABulletsDefault::BulletCollisionSphereHit);
	BulletCollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &ABulletsDefault::BulletCollisionSphereBeginOverlap);
	BulletCollisionSphere->OnComponentEndOverlap.AddDynamic(this, &ABulletsDefault::BulletCollisionSphereEndOverlap);
}

// Called every frame
void ABulletsDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABulletsDefault::InitBullet(FBulletInfo InitParam)
{
	BulletProjectileMovement->InitialSpeed = InitParam.BulletInitSpeed;
	BulletProjectileMovement->MaxSpeed = InitParam.BulletInitSpeed;
	this->SetLifeSpan(InitParam.BulletLifeTime);

	if (InitParam.BulletStaticMesh)
	{
		BulletMesh->SetStaticMesh(InitParam.BulletStaticMesh);
		BulletMesh->SetRelativeTransform(InitParam.BulletStaticMeshOffset);
	}
	else
	{
		BulletMesh->DestroyComponent();
	}

	if (InitParam.BulletTrailFX)
	{
		BulletFX->SetTemplate(InitParam.BulletTrailFX);
		BulletFX->SetRelativeTransform(InitParam.BulletTrailFXOffset);
	}
	else
	{
		BulletFX->DestroyComponent();
	}

	BulletSettings = InitParam;
}

void ABulletsDefault::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor && Hit.PhysMaterial.IsValid())
	{
		EPhysicalSurface mySurfaceType = UGameplayStatics::GetSurfaceType(Hit);

		if (BulletSettings.HitDecals.Contains(mySurfaceType))
		{
			UMaterialInterface* myMaterial = BulletSettings.HitDecals[mySurfaceType];

			if (myMaterial && OtherComp)
			{
				UGameplayStatics::SpawnDecalAttached(myMaterial, FVector(20.0f), OtherComp, NAME_None, Hit.ImpactPoint, Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
			}
		}

		if (BulletSettings.HitFXs.Contains(mySurfaceType))
		{
			UParticleSystem* myParticle = BulletSettings.HitFXs[mySurfaceType];
			if (myParticle)
			{
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), myParticle, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.0f)));
			}
		}

		if (BulletSettings.HitSound)
		{
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), BulletSettings.HitSound, Hit.ImpactPoint);
		}
	
		UTypes::AddEffectBySurfaceType(Hit.GetActor(), BulletSettings.Effect, mySurfaceType);
	}

	UGameplayStatics::ApplyPointDamage(OtherActor, BulletSettings.BulletDamage, Hit.TraceStart, Hit, GetInstigatorController(), this, NULL);
	ImpactProjectile();
}

void ABulletsDefault::BulletCollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
}

void ABulletsDefault::BulletCollisionSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
}

void ABulletsDefault::ImpactProjectile()
{
	this->Destroy();
}

