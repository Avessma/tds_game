// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Particles/ParticleSystemComponent.h"
#include "TDS_StateEffect.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class TDS_GAME_API UTDS_StateEffect : public UObject
{
	GENERATED_BODY()

public:

	virtual bool InitObject(AActor* Actor);
	virtual void DestroyObject();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
	TArray<TEnumAsByte<EPhysicalSurface>> PossibleInteractSurface;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
	bool bIsStakable = false;

	AActor* myActor = nullptr;
};

UCLASS()
class TDS_GAME_API UTDS_StateEffect_ExecuteOnce : public UTDS_StateEffect
{
	GENERATED_BODY()

public:

	bool InitObject(AActor* Actor) override;
	void DestroyObject() override;

	virtual void ExecuteOnce();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
	float Power = 20.0f;
};

UCLASS()
class TDS_GAME_API UTDS_StateEffect_ExecuteTimer : public UTDS_StateEffect
{
	GENERATED_BODY()

public:

	bool InitObject(AActor* Actor) override;
	void DestroyObject() override;

	virtual void Execute();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
	float Power = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
	float Timer = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
	float RateTime = 1.0f;

	FTimerHandle TimerHandle_ExecuteTimer;
	FTimerHandle TimerHandle_EffectTimer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
	UParticleSystem* ParticleEffect = nullptr;

	UParticleSystemComponent* ParticleEmitter = nullptr;
};

UCLASS()
class TDS_GAME_API UTDS_StateEffect_Stan : public UTDS_StateEffect
{
	GENERATED_BODY()

public:

	bool InitObject(AActor* Actor) override;
	void DestroyObject() override;

	virtual void AddStan();
	virtual void OffStan();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
	float Timer = 5.05f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
	float RateTime = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
	float TimerOff = 5.0f;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
	//bool bIsStan = false;
	
	FTimerHandle TimerHandle_EffectTimer;
	FTimerHandle TimerHandle_StanTimer;
	FTimerHandle TimerHandle_StanOffTimer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings")
	UParticleSystem* ParticleEffect = nullptr;

	UParticleSystemComponent* ParticleEmitter = nullptr;
};