// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_Game/Weapons/TDS_StateEffect.h"
#include "TDS_Game/Character/TDS_HelthComponent.h"
#include "TDS_Game/Interface/TDS_IGameActor.h"
#include "TDS_Game/Character/TDS_GameCharacter.h"
#include "TDS_Game/Game/TDS_GamePlayerController.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"

bool UTDS_StateEffect::InitObject(AActor* Actor)
{
	myActor = Actor;

	ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->AddEffect(this);
	}

	return true;
}

void UTDS_StateEffect::DestroyObject()
{
	ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->RemoveEffect(this);
	}

	myActor = nullptr;
	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}
}

bool UTDS_StateEffect_ExecuteOnce::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);
	ExecuteOnce();
	return true;
}

void UTDS_StateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UTDS_StateEffect_ExecuteOnce::ExecuteOnce()
{
	if (myActor)
	{
		UTDS_HelthComponent* myHealthComp = Cast<UTDS_HelthComponent>(myActor->GetComponentByClass(UTDS_HelthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue(Power);
		}
	}

	DestroyObject();
}

bool UTDS_StateEffect_ExecuteTimer::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);

	UE_LOG(LogTemp, Warning, TEXT("UTDS_StateEffect_ExecuteTimer::InitObject"));

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UTDS_StateEffect_ExecuteTimer::DestroyObject, Timer, false);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UTDS_StateEffect_ExecuteTimer::Execute, RateTime, true);

	if (ParticleEffect)
	{
		FName NameBoneToAttached;
		FVector Loc = FVector(0);

		ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(), NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
	}

	return true;
}

void UTDS_StateEffect_ExecuteTimer::DestroyObject()
{
	ParticleEmitter->DestroyComponent();
	ParticleEmitter = nullptr;
	Super::DestroyObject();
}

void UTDS_StateEffect_ExecuteTimer::Execute()
{
	if (myActor)
	{
		UTDS_HelthComponent* myHealthComp = Cast<UTDS_HelthComponent>(myActor->GetComponentByClass(UTDS_HelthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue(Power);
		}
	}
}

bool UTDS_StateEffect_Stan::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);
	
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_StanTimer, this, &UTDS_StateEffect_Stan::AddStan, RateTime, true);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_StanOffTimer, this, &UTDS_StateEffect_Stan::OffStan, TimerOff, true);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UTDS_StateEffect_Stan::DestroyObject, Timer, false);

	if (ParticleEffect)
	{
		FName NameBoneToAttached;
		FVector Loc = FVector(0);

		ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(), NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
	}

	return true;
}

void UTDS_StateEffect_Stan::DestroyObject()
{
	UE_LOG(LogTemp, Warning, TEXT("UTDS_StateEffect_Stan::DestroyObject"));

	if (ParticleEmitter)
	{
		ParticleEmitter->DestroyComponent();
	}
	ParticleEmitter = nullptr;

	Super::DestroyObject();
}

void UTDS_StateEffect_Stan::AddStan()
{
	if (myActor)
	{
		ATDS_GameCharacter* myChar = Cast<ATDS_GameCharacter>(myActor);
		UCharacterMovementComponent* CharMoveComp = myChar->GetCharacterMovement();

		if (CharMoveComp && myChar->bIsAlive)
		{
			CharMoveComp->SetMovementMode(MOVE_None);

			myChar->bIsStan = true;
			UE_LOG(LogTemp, Warning, TEXT("UTDS_StateEffect_Stan::AddStan - bIsStan = true"));
		}
	}	
}

void UTDS_StateEffect_Stan::OffStan()
{
	if (myActor)
	{
		ATDS_GameCharacter* myChar = Cast<ATDS_GameCharacter>(myActor);

		if (myChar)
		{
			UCharacterMovementComponent* CharMoveComp = myChar->GetCharacterMovement();

			if (CharMoveComp)
			{
				myChar->bIsStan = false;
				CharMoveComp->SetMovementMode(MOVE_Walking);
				UE_LOG(LogTemp, Warning, TEXT("UTDS_StateEffect_Stan::OffStan - bIsStan = false"));
			}
		}
	}
}
